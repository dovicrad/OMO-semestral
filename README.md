# Popis projektu

Tento projekt slouží k dokumentaci naši semestrální práce z předmětu OMO na téma **Smart Home**.

Důležité odkazy:
- [Pokyny o semestrálce](https://cw.fel.cvut.cz/wiki/courses/b6b36omo/hw/start)
- [Hodnotící tabulka](https://docs.google.com/spreadsheets/d/1i3d6iiUiubS6MyhwksFV4zR4YdFMqlNn7nJglOKXdV8/edit#gid=0)
- [Zadání Smart Home](https://cw.fel.cvut.cz/wiki/_media/courses/b6b36omo/hw/projekt_smart_home_v2.docx)


Projekt je simulace systému chytré domácnosti. Mezi hlavní součásti projektu patří zařízení, události a samotný dům.

Zařízení: Zařízení, která se nacházejí v domě, jsou určena k instalaci v domě, kde je možné využít tzv: Projekt obsahuje řadu chytrých zařízení, například čistič, kameru, záclonu, dveře, ledničku, plynový kotel, topení, světlo, televizi, termostat, klimatizaci a okno. Každé zařízení je vytvořeno pomocí konstrukčního vzoru, který umožňuje vytváření a konfiguraci zařízení.

Události: Projekt implementuje systém událostí, v němž mohou nastat různé typy událostí a zařízení se mohou k těmto událostem přihlásit. Třída EventManager spravuje odběry a oznámení těchto událostí.

Dům: Dům je reprezentován jako soubor pater, přičemž každé patro obsahuje několik místností. Každá místnost může obsahovat více zařízení. Dům je vytvořen pomocí Factory method, přičemž různé typy domů (jednoduché, velké) jsou vytvářeny různými generátory domů.

Report: generování reportů pro je třeba z domu získat třídu ReportGenerator nebo HouseConfigurationReport. ReportGenerator vypisuje souhrn o zařízeních do konzole a HouseConfigurationReport vytvoří souhrn o domě a a uloží ho do souboru.

Vstupním bodem aplikace je třída SmartHomeApplication. V závislosti na vstupním parametru vytvoří dům pomocí příslušného generátoru domů, provede simulaci a poté vygeneruje zprávu. "big", default -> vytvoří velký dům a "simple" vytvoří menší dům.

# Použité patterny
1. **Vistor** &nbsp; (class: Device a ReportMaker)
    - Využitý pro vytvoření reportu pro zařízení

2. **Singleton** &nbsp; (class: EventManager)
    - Pro sjednocení eventů mezi třídy

3. **Observer** &nbsp; (class: EventManager a Subscriber)
    - Využitý pro sledování a propagaci eventů
    - Device má ref na EventManager, kterým propaguje eventy

4. **Iterator**  &nbsp; (class: DeviceIterator)
    - Využitý pro iteraci přes zařízení v domácnosti
    - vrací ho metoda getDeviceIterator v třídě House

5. **Factory Method** &nbsp; (class: HouseGenerator, BigHouseGenerator, SimpleHouseGenerator)
    - Využitý pro tvorbu domů

6. **Builder** &nbsp; (class: DeviceBuilder, EquipmentBuilder)
    - Využitý pro tvorbu zařízení a vybavení v domě

7. **State** &nbsp; (class: DeviceState, GasDeviceState, ElectricDeviceState...)
    - Využitý pro výpočet spotřeby zařízení

# Use case model
![use-case.png](resources%2Fuse-case.png)

# Class diagram
![class diagram.png](resources%2Fclass%20diagram.png)
![pattern diagram.png](resources%2Fpattern%20diagram.png)