package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirConditioner implements DeviceBuilder {
    String name;
    Integer wattage;

    public AirConditioner setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public AirConditioner setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public AirConditioner build(Director director) {
        director.buildAirConditioner(this);
        return this;
    }

    public cz.cvut.fel.omo.smarthome.model.device.electric.AirConditioner getResult() {
        log.info("A new voice assistant was created by VoiceAssistantBuilder.");
        return new cz.cvut.fel.omo.smarthome.model.device.electric.AirConditioner(name, wattage);
    }
}
