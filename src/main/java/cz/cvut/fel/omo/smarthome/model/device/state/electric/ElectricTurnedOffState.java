package cz.cvut.fel.omo.smarthome.model.device.state.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.ElectricDevice;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
public class ElectricTurnedOffState extends ElectricDeviceState {

    public ElectricTurnedOffState(ElectricDevice device) {
        super(device);
    }

    @Override
    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now) {
        return 0;
    }

    @Override
    public void turnOn() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to on");
        device.setState(new ElectricTurnedOnState(device));
    }

    @Override
    public void turnOff() {
        log.info(device.getDeviceName() + " is already turned off");
    }

    @Override
    public void idle() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to idle");
        device.setState(new ElectricIdleState(device));
    }
}
