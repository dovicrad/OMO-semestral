package cz.cvut.fel.omo.smarthome.utils.builder.gas;

import cz.cvut.fel.omo.smarthome.model.device.gas.Heater;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HeaterBuilder implements DeviceBuilder {
    String name;
    Integer gas;

    public HeaterBuilder setGas(Integer gas) {
        this.gas = gas;
        return this;
    }

    @Override
    public HeaterBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public HeaterBuilder build(Director director) {
       director.buildHeater(this);
        return this;
    }

    public Heater getResult() {
        log.info("A new heater was created by HeaterBuilder.");
        return new Heater(name, gas);
    }
}
