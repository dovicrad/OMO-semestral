package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Curtain extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();
    public Curtain(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(EventType.TURN_ON_AUTO_CURTAINS, this);
        eventManager.subscribe(EventType.TURN_OFF_AUTO_CURTAINS, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case TURN_ON_AUTO_CURTAINS:
                this.getState().turnOn();
                break;
            case TURN_OFF_AUTO_CURTAINS:
                this.getState().idle();
                break;
            default:
               log.info("I cannot handle this event.");
        }

    }
}
