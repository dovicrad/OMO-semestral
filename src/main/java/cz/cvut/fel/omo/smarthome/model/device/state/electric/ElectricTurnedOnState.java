package cz.cvut.fel.omo.smarthome.model.device.state.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.ElectricDevice;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ElectricTurnedOnState extends ElectricDeviceState {

    public ElectricTurnedOnState(ElectricDevice device) {
        super(device);
    }

    @Override
    public void turnOn() {
        log.info(device.getDeviceName()+" is already turned on");
    }

    @Override
    public void turnOff() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName()+" switched state to off");
        device.setState(new ElectricTurnedOffState(device));
    }

    @Override
    public void idle() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName()+" switched state to idle");
        device.setState(new ElectricIdleState(device));
    }
}
