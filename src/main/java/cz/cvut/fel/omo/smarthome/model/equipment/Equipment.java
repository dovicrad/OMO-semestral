package cz.cvut.fel.omo.smarthome.model.equipment;

import cz.cvut.fel.omo.smarthome.model.resident.Person;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * Represents equipment in the house with ownership tracking.
 */
@Slf4j
@Getter
public abstract class Equipment {
    private Person currentOwner;
    private final String equipmentName;
    private final Color color;
    private final String brand;

    protected Equipment(String equipmentName, Color color, String brand) {
        this.equipmentName = equipmentName;
        this.color = color;
        this.brand = brand;
    }

    public boolean isAvailable() {
        return Objects.isNull(currentOwner);
    }

    public void setCurrentOwner(Person currentOwner) {
        if(isAvailable()){
            this.currentOwner = currentOwner;
            log.info(equipmentName + " is now used by " + currentOwner.getName());
        } else {
            log.info(equipmentName + " is not available, because it's already used by " + currentOwner.getName());
        }
    }

    public void returnEquipment(){
        if(!isAvailable()){
            log.info(equipmentName + " is now available");
            currentOwner = null;
        } else {
            log.info(equipmentName + " is already available");
        }
    }
}
