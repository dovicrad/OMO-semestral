package cz.cvut.fel.omo.smarthome.model.event;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
/**
 * Singleton class managing event subscribers and notifications.
 * Implements the Observer pattern.
 */
@Slf4j
public final class EventManager {
    private static final Map<EventType, Set<Subscriber>> subscribers = new EnumMap<>(EventType.class);

    private static EventManager instance = null;
    private EventManager(){

    }

    public static EventManager getInstance() {
        if(instance == null) {
            instance = new EventManager();
        }
        return instance;
    }

    /**
     * Subscribes a subscriber to an event.
     * @param event event to subscribe to
     * @param subscriber subscriber to subscribe
     */
    public void subscribe(EventType event, Subscriber subscriber) {
        if (subscribers.containsKey(event)) {
            subscribers.get(event).add(subscriber);
        } else {
            Set<Subscriber> newSubscribersSet = new HashSet<>();
            newSubscribersSet.add(subscriber);
            subscribers.put(event, newSubscribersSet);
        }
    }

    /**
     * Unsubscribes a subscriber from an event.
     * @param event event to unsubscribe from
     * @param subscriber subscriber to unsubscribe
     */
    public void unsubscribe(EventType event, Subscriber subscriber) {
        if (subscribers.containsKey(event)) {
            subscribers.get(event).remove(subscriber);
        }
    }

    /**
     * Notifies all subscribers of an event.
     * @param event event to notify subscribers of
     */
    public void notifySubscribers(EventType event) {
        log.info("Event is happening: " + event);
        if (subscribers.containsKey(event) && !subscribers.get(event).isEmpty()) {
            for (Subscriber subscriber : subscribers.get(event)) {
                subscriber.update(event);
            }
        }
        else{
            log.info("No subscribers for event: " + event);
        }
    }
}
