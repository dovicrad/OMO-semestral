package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.device.state.electric.ElectricDeviceState;
import cz.cvut.fel.omo.smarthome.model.device.state.electric.ElectricTurnedOffState;
import cz.cvut.fel.omo.smarthome.report.DeviceReportGenerator;
import cz.cvut.fel.omo.smarthome.utils.CustomClock;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * Represents an electric device in a smart home.
 * Inherits from Device and adds electricity consumption tracking.
 */
@Slf4j
@Getter
@Setter
public abstract class ElectricDevice extends Device {
    private final Integer wattage;
    private ElectricDeviceState state;
    private double totalEnergyConsumption = 0.0;
    private LocalDateTime lastSwitched;

    protected ElectricDevice(String deviceName, Integer wattage) {
        super(deviceName);
        this.wattage = wattage;
        this.lastSwitched = LocalDateTime.now(CustomClock.getClock());
        this.state = new ElectricTurnedOffState(this);
    }

    @Override
    public void accept(DeviceReportGenerator visitor) {
        visitor.generateReport(this);
    }

    /**
     * Calculates electricity consumption of the device in Wh
     * @return electricity consumption in Wh
     */
    public double getElectricityConsumption(){
        return totalEnergyConsumption + getConsumptionFromLastSwitch();
    }

    /**
     * Calculates the electricity consumption from the last switch to now
     * @return electricity consumption in Wh
     */
    private double getConsumptionFromLastSwitch() {
        return state.getConsumptionBetween(lastSwitched, LocalDateTime.now(CustomClock.getClock()));
    }
}
