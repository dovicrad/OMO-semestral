package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Door extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();
    public Door(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(EventType.ENABLE_HOUSE_SECURITY, this);
        eventManager.subscribe(EventType.DISABLE_HOUSE_SECURITY, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case ENABLE_HOUSE_SECURITY:
                this.getState().turnOn();
                break;
            case DISABLE_HOUSE_SECURITY:
                this.getState().idle();
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
