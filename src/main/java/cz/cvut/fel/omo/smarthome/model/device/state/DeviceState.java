package cz.cvut.fel.omo.smarthome.model.device.state;


import java.time.LocalDateTime;

/**
 * Interface for device states
 * each of the device states has to implement this interface
 */
public interface DeviceState {

    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now);

    public void turnOn();

    public void turnOff();

    public void idle();
}