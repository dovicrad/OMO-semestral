package cz.cvut.fel.omo.smarthome.model.device;

import cz.cvut.fel.omo.smarthome.report.DeviceReportGenerator;
import lombok.Getter;

/**
 * Represents a device in a smart home with a name and a document (manual).
 */
@Getter
public abstract class Device {
    protected String deviceName;
    protected final Document document;

    /**
     * Accepts a visitor to generate a report.
     * @param visitor the visitor
     */
    public abstract void accept(DeviceReportGenerator visitor);

    protected Device(String deviceName){
        this.deviceName = deviceName;
        document = new Document(deviceName);
    }
}
