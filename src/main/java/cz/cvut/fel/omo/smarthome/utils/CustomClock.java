package cz.cvut.fel.omo.smarthome.utils;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import lombok.Getter;

import java.time.Clock;

/**
 * This class represents a custom clock that is used to simulate the passing of time.
 */
public final class CustomClock {
    @Getter
    private static Clock clock = Clock.systemDefaultZone();

    private static final EventManager eventManager = EventManager.getInstance();

    private CustomClock() {}

    public static void resetClock() {
        clock = Clock.systemDefaultZone();
    }

    public static void forwardTimeByHour() {
        clock = Clock.offset(clock, java.time.Duration.of(1, java.time.temporal.ChronoUnit.HOURS));
        eventManager.notifySubscribers(EventType.HOUR_PASSED);
    }
    public static void forwardTimeByHour(Integer hours) {
        clock = Clock.offset(clock, java.time.Duration.of(hours, java.time.temporal.ChronoUnit.HOURS));
        for (int i = 0; i < hours; i++){
            eventManager.notifySubscribers(EventType.HOUR_PASSED);
        }
    }
}
