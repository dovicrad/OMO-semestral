package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Curtain;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CurtainBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public CurtainBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public CurtainBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public CurtainBuilder build(Director director) {
        director.buildCurtain(this);
        return this;
    }

    public Curtain getResult() {
        log.info("A new curtain was created by CurtainBuilder.");
        return new Curtain(name, wattage);
    }
}
