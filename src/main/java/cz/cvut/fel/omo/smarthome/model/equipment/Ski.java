package cz.cvut.fel.omo.smarthome.model.equipment;

public class Ski extends Equipment {

    public Ski(Color color, String brand) {
        super("Ski", color, brand);
    }
}
