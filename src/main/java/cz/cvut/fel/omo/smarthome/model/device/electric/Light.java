package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

import static cz.cvut.fel.omo.smarthome.model.event.EventType.TURN_OFF_LIGHTS;
import static cz.cvut.fel.omo.smarthome.model.event.EventType.TURN_ON_LIGHTS;

@Slf4j
public class Light extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();
 
    public Light(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(TURN_ON_LIGHTS, this);
        eventManager.subscribe(TURN_OFF_LIGHTS, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case TURN_ON_LIGHTS:
                this.getState().turnOn();
                break;
            case TURN_OFF_LIGHTS:
                this.getState().turnOff();
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
