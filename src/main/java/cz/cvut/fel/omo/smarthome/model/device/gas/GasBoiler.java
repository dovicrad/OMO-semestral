package cz.cvut.fel.omo.smarthome.model.device.gas;

public class GasBoiler extends GasDevice {
    public GasBoiler(String deviceName, Integer gas) {
        super(deviceName, gas);
    }
}
