package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Window;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WindowBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public WindowBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public WindowBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public WindowBuilder build(Director director) {
        director.buildWindow(this);
        return this;
    }

    public Window getResult() {
        log.info("A new window was created by WindowBuilder.");
        return new Window(name, wattage);
    }
}
