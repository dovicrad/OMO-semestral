package cz.cvut.fel.omo.smarthome.model.device.gas;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.device.state.gas.GasDeviceState;
import cz.cvut.fel.omo.smarthome.model.device.state.gas.GasTurnedOffState;
import cz.cvut.fel.omo.smarthome.report.DeviceReportGenerator;
import cz.cvut.fel.omo.smarthome.utils.CustomClock;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Represents device powered by gas in a smart home.
 * Inherits from Device and adds gas consumption tracking.
 */
@Getter
@Setter
public abstract class GasDevice extends Device {
    private final double gas;
    private GasDeviceState state;
    private double totalGasConsumption = 0.0;
    private LocalDateTime lastSwitched;

    protected GasDevice(String deviceName, double gas) {
        super(deviceName);
        this.gas = gas;
        this.lastSwitched = LocalDateTime.now(CustomClock.getClock());
        this.state = new GasTurnedOffState(this);
    }

    @Override
    public void accept(DeviceReportGenerator visitor) {
        visitor.generateReport(this);
    }

    /**
     * Calculates the gas consumption of the device in m^3
     * @return gas consumption in m^3
     */
    public double getGasConsumption(){
        return totalGasConsumption + getConsumptionFromLastSwitch();
    }

    /**
     * Calculates the gas consumption from the last switch to now
     * @return gas consumption in m^3
     */
    private double getConsumptionFromLastSwitch() {
        return state.getConsumptionBetween(lastSwitched, LocalDateTime.now(CustomClock.getClock()));
    }
}
