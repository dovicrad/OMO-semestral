package cz.cvut.fel.omo.smarthome.model.device.state.gas;

import cz.cvut.fel.omo.smarthome.model.device.gas.GasDevice;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GasTurnedOnState extends GasDeviceState {
    public GasTurnedOnState(GasDevice device) {
        super(device);
    }

    @Override
    public void idle() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to idle");
        device.setState(new GasIdleState(device));
    }

    @Override
    public void turnOff() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to off");
        device.setState(new GasTurnedOffState(device));
    }

    @Override
    public void turnOn() {
        log.info("Device is already turned on");
    }
}
