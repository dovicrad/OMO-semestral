package cz.cvut.fel.omo.smarthome.model.event;

/**
 * Interface for receiving event updates.
 * Part of the observer pattern
 */
public interface Subscriber {
    void update(EventType event);
}
