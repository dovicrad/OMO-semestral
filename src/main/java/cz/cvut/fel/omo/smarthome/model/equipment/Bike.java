package cz.cvut.fel.omo.smarthome.model.equipment;

public class Bike extends Equipment {
    public Bike(Color color, String brand) {
        super("Bike", color, brand);
    }
}
