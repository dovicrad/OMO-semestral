package cz.cvut.fel.omo.smarthome.model.device.state.gas;

import cz.cvut.fel.omo.smarthome.model.device.gas.GasDevice;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
public class GasTurnedOffState extends GasDeviceState {
    public GasTurnedOffState(GasDevice device) {
        super(device);
    }

    @Override
    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now) {
        return 0;
    }

    @Override
    public void idle() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName()+" switched state to idle");
        device.setState(new GasIdleState(device));
    }

    @Override
    public void turnOff() {
       log.info(device.getDeviceName() + " is already turned off");
    }

    @Override
    public void turnOn() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName()+" switched state to on");
        device.setState(new GasTurnedOnState(device));
    }
}
