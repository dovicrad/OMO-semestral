package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Television;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TelevisionBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public TelevisionBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public TelevisionBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public TelevisionBuilder build(Director director) {
        director.buildTelevision(this);
        return this;
    }

    public Television getResult() {
        log.info("A new television was created by TelevisionBuilder.");
        return new Television(name, wattage);
    }
}
