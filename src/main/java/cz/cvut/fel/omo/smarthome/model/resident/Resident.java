package cz.cvut.fel.omo.smarthome.model.resident;

import cz.cvut.fel.omo.smarthome.model.house.Room;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a resident of the house.
 */
@Getter
@Setter
public abstract class Resident {
    private final String name;
    private Room locatedIn;

    protected Resident(String name, Room locatedIn) {
        this.name = name;
        this.locatedIn = locatedIn;
    }
}
