package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Cleaner;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CleanerBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public CleanerBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public CleanerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public CleanerBuilder build(Director director) {
        director.buildCleaner(this);
        return this;
    }

    public Cleaner getResult() {
        log.info("A new cleaner was created by CleanerBuilder.");
        return new Cleaner(name, wattage);
    }
}
