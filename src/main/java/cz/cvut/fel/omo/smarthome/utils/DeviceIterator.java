package cz.cvut.fel.omo.smarthome.utils;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.house.Floor;
import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.model.house.Room;

import java.util.List;

/**
 * This class represents a DeviceIterator in the Iterator design pattern.
 * It provides methods to iterate over all devices in a house.
 */
public class DeviceIterator {
    private final House house;
    private int floorIndex;
    private int roomIndex;
    private int deviceIndex;

    public DeviceIterator(House house){
        this.house = house;
        this.floorIndex = 0;
        this.roomIndex = 0;
        this.deviceIndex = 0;
    }

    public void reset(){
        this.floorIndex = 0;
        this.roomIndex = 0;
        this.deviceIndex = 0;
    }

    public boolean hasNext() {
        List<Floor> floors = house.getFloors();
        while (floorIndex < floors.size()) {
            List<Room> rooms = floors.get(floorIndex).getRooms();
            while (roomIndex < rooms.size()) {
                List<Device> devices = rooms.get(roomIndex).getDevices();
                if (deviceIndex < devices.size()) {
                    return true;
                }
                deviceIndex = 0;
                roomIndex++;
            }
            roomIndex = 0;
            floorIndex++;
        }
        return false;
    }

    public Device next() {
        if (hasNext()) {
            return house.getFloors().get(floorIndex).getRooms().get(roomIndex).getDevices().get(deviceIndex++);
        }
        return null;
    }
}