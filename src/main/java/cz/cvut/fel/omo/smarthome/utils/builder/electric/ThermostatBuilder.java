package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Thermostat;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThermostatBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public ThermostatBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public ThermostatBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public ThermostatBuilder build(Director director) {
        director.buildThermostat(this);
        return this;
    }

    public Thermostat getResult() {
        log.info("A new thermostat was created by ThermostatBuilder.");
        return new Thermostat(name, wattage);
    }
}
