package cz.cvut.fel.omo.smarthome.utils.factory;

import cz.cvut.fel.omo.smarthome.model.equipment.Bike;
import cz.cvut.fel.omo.smarthome.model.equipment.Color;
import cz.cvut.fel.omo.smarthome.model.equipment.Ski;
import cz.cvut.fel.omo.smarthome.model.house.Floor;
import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.model.house.Room;
import cz.cvut.fel.omo.smarthome.model.resident.Person;
import cz.cvut.fel.omo.smarthome.model.resident.Pet;

import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

public class SimpleHouseGenerator extends HouseGenerator    {
    private static final Integer NUMBER_OF_FLOORS = 1;
    private static final Integer NUMBER_OF_ROOMS_PER_FLOOR = 5;
    private static final Integer NUMBER_OF_DEVICES = 10;
    private static final Integer NUMBER_OF_DEVICE_TYPES = 3;

    private static final Integer NUMBER_OF_PEOPLE = 3;
    private static final List<String> PEOPLE_NAMES = List.of("Aria", "Xavier", "Isabella");
    private static final Integer NUMBER_OF_PETS = 1;
    private static final List<String> PET_NAMES = List.of("Azor");


    @Override
    public House createHouse() {
        House house = super.buildHouseWithDevices(NUMBER_OF_FLOORS, NUMBER_OF_ROOMS_PER_FLOOR, NUMBER_OF_DEVICES, NUMBER_OF_DEVICE_TYPES);
        house.setEquipment(Set.of(
                new Ski(Color.BLACK, "Atomic"),
                new Bike(Color.GREEN,"Canyon")
        ));
        assignResidentsToRooms(house, NUMBER_OF_PEOPLE, (i, room) -> new Person(PEOPLE_NAMES.get(i), room));
        assignResidentsToRooms(house, NUMBER_OF_PETS, (i, room) -> new Pet(PET_NAMES.get(i), i, "Dog", room));

        return house;
    }
    private void assignResidentsToRooms(House house, int numberOfResidents, BiFunction<Integer, Room, ?> resident) {
        for (int i = 0; i < numberOfResidents; i++) {
            Floor floor = house.getFloors().get(i / NUMBER_OF_ROOMS_PER_FLOOR);
            Room room = floor.getRooms().get(i % NUMBER_OF_ROOMS_PER_FLOOR);
            resident.apply(i, room);
        }
    }
}
