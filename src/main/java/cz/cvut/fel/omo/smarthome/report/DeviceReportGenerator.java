package cz.cvut.fel.omo.smarthome.report;

import cz.cvut.fel.omo.smarthome.model.device.electric.ElectricDevice;
import cz.cvut.fel.omo.smarthome.model.device.gas.GasDevice;

/**
 * Interface for generating device-specific reports.
 * Part of the visitor pattern
 */
public interface DeviceReportGenerator {
    void generateReport(ElectricDevice electricDevice);
    void generateReport(GasDevice gasDevice);
}
