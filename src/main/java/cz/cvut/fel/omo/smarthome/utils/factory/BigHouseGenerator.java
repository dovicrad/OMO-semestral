package cz.cvut.fel.omo.smarthome.utils.factory;

import cz.cvut.fel.omo.smarthome.model.equipment.Bike;
import cz.cvut.fel.omo.smarthome.model.equipment.Color;
import cz.cvut.fel.omo.smarthome.model.equipment.Ski;
import cz.cvut.fel.omo.smarthome.model.house.Floor;
import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.model.house.Room;
import cz.cvut.fel.omo.smarthome.model.resident.Person;
import cz.cvut.fel.omo.smarthome.model.resident.Pet;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

/**
 *  Minimální konfigurace alespoň jednoho domu je:
 *  6 osob - Aria, Xavier, Isabella, Leo, Zoe, Caleb
 *  3 zvířata,
 *  8 typů spotřebičů | DISCLAIMER: we will create one of each available type! (12) |,
 *  20 ks spotřebičů,
 *  6 místností,
 *  jedny lyže, dvě kola.
 */

@Slf4j
public class BigHouseGenerator extends HouseGenerator {
    private static final Integer NUMBER_OF_FLOORS = 2;
    private static final Integer NUMBER_OF_ROOMS_PER_FLOOR = 3;
    private static final Integer NUMBER_OF_DEVICES = 20;
    private static final Integer NUMBER_OF_DEVICE_TYPES = BUILDERS.size();
    private static final Integer NUMBER_OF_PEOPLE = 6;
    private static final List<String> PEOPLE_NAMES = List.of("Aria", "Xavier", "Isabella", "Leo", "Zoe", "Caleb");
    private static final Integer NUMBER_OF_PETS = 3;

    @Override
    public House createHouse() {
        House house = super.buildHouseWithDevices(NUMBER_OF_FLOORS, NUMBER_OF_ROOMS_PER_FLOOR, NUMBER_OF_DEVICES, NUMBER_OF_DEVICE_TYPES);
        house.setEquipment(Set.of(
                new Ski(Color.BLACK, "Atomic"),
                new Bike(Color.BLUE, "Trek"),
                new Bike(Color.GREEN,"Canyon")
        ));

        assignResidentsToRooms(house, NUMBER_OF_PEOPLE, (i, room) -> new Person(PEOPLE_NAMES.get(i), room));
        assignResidentsToRooms(house, NUMBER_OF_PETS, (i, room) -> new Pet("Pet", i, "Dog", room));

        return house;
    }
    private void assignResidentsToRooms(House house, int numberOfResidents, BiFunction<Integer, Room, ?> resident) {
        for (int i = 0; i < numberOfResidents; i++) {
            Floor floor = house.getFloors().get(i / NUMBER_OF_ROOMS_PER_FLOOR);
            Room room = floor.getRooms().get(i % NUMBER_OF_ROOMS_PER_FLOOR);
            resident.apply(i, room);
        }
    }
}
