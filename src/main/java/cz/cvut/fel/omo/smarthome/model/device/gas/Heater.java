package cz.cvut.fel.omo.smarthome.model.device.gas;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Heater extends GasDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    public Heater(String deviceName, Integer gas) {
        super(deviceName, gas);
        eventManager.subscribe(EventType.TURN_ON_HEATING, this);
        eventManager.subscribe(EventType.TURN_OFF_HEATING, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case TURN_ON_HEATING:
                this.getState().turnOn();
                break;
            case TURN_OFF_HEATING:
                this.getState().turnOff();
                break;
            default:
                log.info("I cannot handle this event");
        }
    }
}
