package cz.cvut.fel.omo.smarthome.model.equipment;

public enum Color {
    RED,
    BLUE,
    GREEN,
    YELLOW,
    ORANGE,
    WHITE,
    BLACK
}
