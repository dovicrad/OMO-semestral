package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Fridge;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FridgeBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public FridgeBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public FridgeBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public FridgeBuilder build(Director director) {
        director.buildFridge(this);
        return this;
    }

    public Fridge getResult() {
        log.info("A new fridge was created by FridgeBuilder.");
        return new Fridge(name, wattage);
    }
}
