package cz.cvut.fel.omo.smarthome.model.house;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.resident.Person;
import cz.cvut.fel.omo.smarthome.model.resident.Resident;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Room {
    private final RoomType roomType;
    private final List<Resident> residents;
    private final List<Device> devices;
    private final Floor floor;

    public Room(RoomType roomType, Floor floor) {
        this.devices = new ArrayList<>();
        this.residents = new ArrayList<>();
        this.roomType = roomType;
        this.floor = floor;
    }

    public List<Resident> getResidents() {
        return new ArrayList<>(residents);
    }

    public void addResident(Person person) {
        residents.add(person);
    }

    public void removeResident(Resident person) {
        residents.remove(person);
    }

    public List<Device> getDevices() {
        return new ArrayList<>(devices);
    }

    public void addDevice(Device device) {
        devices.add(device);
    }
    public void addDevice(List<Device> devices) {
        this.devices.addAll(devices);
    }

    public void removeDevice(Device device) {
        devices.remove(device);
    }

    public RoomType getRoomType() {
        return roomType;
    }
}
