package cz.cvut.fel.omo.smarthome.model.house;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Floor {
    public Floor(House house) {
        this.house = house;
    }
    public Floor(List<Room> rooms, House house) {
        this.rooms = new ArrayList<>();
        this.house = house;
    }
    private House house;
    private List<Room> rooms = new ArrayList<>();
}
