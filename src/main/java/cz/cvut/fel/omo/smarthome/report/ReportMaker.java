package cz.cvut.fel.omo.smarthome.report;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.device.electric.ElectricDevice;
import cz.cvut.fel.omo.smarthome.model.device.gas.GasDevice;
import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.utils.DeviceIterator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Generates a report of all devices in a house.
 */
@Getter
@Setter
@Slf4j
@NoArgsConstructor
public class ReportMaker implements DeviceReportGenerator {
    private House myHouse;
    float totalGasConsumption = 0;
    float totalElectricConsumption = 0;
    private static final String DELIMITER = "#######################################################################################";

    public ReportMaker(House house) {
        this.myHouse = house;
    }

    public void makeReport() {
        DeviceIterator iterator = myHouse.getDeviceIterator();
        while (iterator.hasNext()) {
            Device device = iterator.next();
            device.accept(this);
        }
        log.info("\n"+DELIMITER);
        log.info("Total gas consumption: " + String.format("%.2f", totalGasConsumption) + " m3");
        log.info("Total electric consumption: " + String.format("%.2f", totalElectricConsumption) + " Wh");
    }

    @Override
    public void generateReport(ElectricDevice electricDevice) {
        log.info("Electric device: " + electricDevice.getDeviceName() + ", consumption: " + String.format("%.2f", electricDevice.getElectricityConsumption()) + " Wh");
        totalElectricConsumption += (float) electricDevice.getElectricityConsumption();
    }
    @Override
    public void generateReport(GasDevice gasDevice) {
        log.info("Gas device: " + gasDevice.getDeviceName() + ", consumption: " + String.format("%.2f", gasDevice.getGasConsumption()) + " m3");
        totalGasConsumption += (float) gasDevice.getGasConsumption();
    }
}
