package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class Window extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    private double rollerBlindsPosition = 0.0;

    public Window(String deviceName, Integer wattage) {
        super(deviceName,wattage);
        eventManager.subscribe(EventType.NIGHT, this);
        eventManager.subscribe(EventType.DAY, this);
        eventManager.subscribe(EventType.SUNNY, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case NIGHT:
                rollerBlindsPosition = 1.0;
                log.info("Closing roller blinds.");
                break;
            case DAY:
                rollerBlindsPosition = 0.0;
                log.info("Opening roller blinds.");
                break;
            case SUNNY:
                rollerBlindsPosition = 0.5;
                log.info("Setting roller blinds to half.");
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
