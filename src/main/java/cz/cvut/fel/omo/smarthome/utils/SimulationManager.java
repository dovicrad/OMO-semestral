package cz.cvut.fel.omo.smarthome.utils;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import lombok.extern.slf4j.Slf4j;

/**
 * This class is responsible for simulating events in the smart home.
 */
@Slf4j
public class SimulationManager {
    private static final EventManager eventManager = EventManager.getInstance();

    private SimulationManager() {}

    public static void simulate(){
        eventManager.notifySubscribers(EventType.NIGHT);
        eventManager.notifySubscribers(EventType.BAD_HOUSE_TEMPERATURE);
        eventManager.notifySubscribers(EventType.INTRUDER_CAME);
        CustomClock.forwardTimeByHour();
        eventManager.notifySubscribers(EventType.INTRUDER_LEFT);
        eventManager.notifySubscribers(EventType.COLD);
        CustomClock.forwardTimeByHour(5);
        eventManager.notifySubscribers(EventType.HOT);
        CustomClock.forwardTimeByHour();
        eventManager.notifySubscribers(EventType.TURN_OFF_AUTO_CURTAINS);
        CustomClock.forwardTimeByHour();

        eventManager.notifySubscribers(EventType.DAY);
        eventManager.notifySubscribers(EventType.SUNNY);
    }
}
