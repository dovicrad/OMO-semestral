package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Light;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LightBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public LightBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public LightBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public LightBuilder build(Director director) {
        director.buildLight(this);
        return this;
    }

    public Light getResult() {
        log.info("A new light device was created by LightBuilder.");
        return new Light(name, wattage);
    }
}
