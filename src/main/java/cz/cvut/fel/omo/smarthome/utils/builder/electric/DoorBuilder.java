package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Door;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DoorBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    public DoorBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    @Override
    public DoorBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public DoorBuilder build(Director director) {
        director.buildDoor(this);
        return this;
    }

    public Door getResult() {
        log.info("A new door was created by DoorBuilder.");
        return new Door(name, wattage);
    }
}
