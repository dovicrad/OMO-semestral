package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.device.FridgeItem;
import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.smarthome.model.event.EventType.CHECK_FRIDGE;

@Slf4j
public class Fridge extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    private double fillPercentage;
    private static final double FRIDGE_SIZE = 1.0; // 100%

    List<FridgeItem> fridgeItems;


    public Fridge(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        fillPercentage = 0.0;
        fridgeItems = new ArrayList<>();
        eventManager.subscribe(CHECK_FRIDGE, this);
    }


    @Override
    public void update(EventType event) {
        switch (event){
            case CHECK_FRIDGE:
                log.info("Fridge is "+(fillPercentage*100)+"% full");
                break;
            default:
                log.info("I cannot handle this event");
        }
    }

    public double getFridgeSpace() {
        return fillPercentage;
    }

    public boolean addItem(FridgeItem item) {
        // Fridge still have space, return true when successfully added
        if (fillPercentage + item.getSize() < FRIDGE_SIZE) {
            log.info("Item " + item.getName() + " is added to the fridge, fridge is currently " + (fillPercentage*100) + "% full");
            return true;
        }
        log.info("Can't add " + item.getName() + " to the fridge, there is not enough space there.");
        return false;
    }

    public boolean isEmpty() { return fillPercentage == 0.0; }

    public void clearFridge() {
        fridgeItems.clear();
        log.info("All items has been removed from the fridge. Fridge is currently empty.");
    }

    public void addPresetItems() {
        fridgeItems.add(new FridgeItem("Eggs", 0.05));
        fridgeItems.add(new FridgeItem("Milk", 0.06));
        fridgeItems.add(new FridgeItem("Wine", 0.07));
        fridgeItems.add(new FridgeItem("Beef", 0.1));
        fridgeItems.add(new FridgeItem("Chicken soup", 0.2));
    }
}
