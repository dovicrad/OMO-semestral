package cz.cvut.fel.omo.smarthome.utils.builder;


import cz.cvut.fel.omo.smarthome.model.device.Device;

/**
 * This interface is a part of Builder design pattern.
 * It provides methods to build a device.
 */
public interface DeviceBuilder {
    DeviceBuilder setName(String name);
    DeviceBuilder build(Director director);
    Device getResult();
}
