package cz.cvut.fel.omo.smarthome.model.device.state.gas;

import cz.cvut.fel.omo.smarthome.model.device.gas.GasDevice;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
public class GasIdleState extends GasDeviceState {
    private static final double IDLE_CONSUMPTION_FACTOR = 0.1;
    public GasIdleState(GasDevice device) {
        super(device);
    }

    @Override
    public void idle() {
        log.info(device.getDeviceName() + " is already idle");
    }

    @Override
    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now) {
        return super.getConsumptionBetween(lastSwitched, now) * IDLE_CONSUMPTION_FACTOR;
    }

    @Override
    public void turnOff() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to off");
        device.setState(new GasTurnedOffState(device));
    }

    @Override
    public void turnOn() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to on");
        device.setState(new GasTurnedOnState(device));
    }

}
