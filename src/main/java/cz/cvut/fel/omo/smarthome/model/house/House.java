package cz.cvut.fel.omo.smarthome.model.house;

import cz.cvut.fel.omo.smarthome.model.equipment.Equipment;
import cz.cvut.fel.omo.smarthome.report.HouseConfigurationReport;
import cz.cvut.fel.omo.smarthome.report.ReportMaker;
import cz.cvut.fel.omo.smarthome.utils.DeviceIterator;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class House {
    private String address;
    private List<Floor> floors;
    private Set<Equipment> equipment;

    public House() {
        floors = new ArrayList<>();
        equipment = new HashSet<>();
    }

    public House(List<Floor> floors) {
        this.floors = floors;
        equipment = new HashSet<>();
    }

    public DeviceIterator getDeviceIterator() {
        return new DeviceIterator(this);
    }

    public ReportMaker getReportMaker() {
        return new ReportMaker(this);
    }

    public List<Floor> getFloors() {return floors;}

    public HouseConfigurationReport getHouseConfigurationReport() {
        return new HouseConfigurationReport(this);
    }
}
