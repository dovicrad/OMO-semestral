package cz.cvut.fel.omo.smarthome.report;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.house.Floor;
import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.model.house.Room;
import lombok.extern.slf4j.Slf4j;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Generates a report about the configuration of the house.
 */
@Slf4j
public class HouseConfigurationReport {
    private final House house;
    private final String fileName;

    public HouseConfigurationReport (House house) {
        this.house = house;
        fileName = "reports/HouseConfigurationReport_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_HH_mm_ss")) + ".txt";
    }

    public void generate() {
        try {
            FileWriter writer = new FileWriter(fileName);
            writer.write(getDataFromHouse());
            writer.close();

            log.info("HouseConfigurationReport generated successfully. Check reports directory.");
        } catch (IOException e) {
            log.info("Error while generating the HouseConfigurationReport.");

            e.printStackTrace();
        }
    }

    private String getDataFromHouse() {
        String dataForReport = "";

        dataForReport += "House configuration report:\n";

        // For each floor in the house
        for (int floorIndex = 0; floorIndex < house.getFloors().size(); floorIndex++) {
            Floor currentFloor = house.getFloors().get(floorIndex);
            dataForReport += "   Floor " + floorIndex + ":\n";
            for (int roomIndex = 0; roomIndex < currentFloor.getRooms().size(); roomIndex++) {
                Room currentRoom = currentFloor.getRooms().get(roomIndex);
                dataForReport += "      " + currentRoom.getRoomType() + ":";
                for (int deviceIndex = 0; deviceIndex < currentRoom.getDevices().size(); deviceIndex++) {
                    Device currentDevice = currentRoom.getDevices().get(deviceIndex);
                    dataForReport += " " + currentDevice.getDeviceName() + ",";
                }
                dataForReport += "\n";
            }
            dataForReport += "\n";
        }

        return dataForReport;
    }
}
