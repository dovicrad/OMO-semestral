package cz.cvut.fel.omo.smarthome.model.device.state.gas;

import cz.cvut.fel.omo.smarthome.model.device.gas.GasDevice;
import cz.cvut.fel.omo.smarthome.model.device.state.DeviceState;
import cz.cvut.fel.omo.smarthome.utils.CustomClock;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class GasDeviceState implements DeviceState {
    protected GasDevice device;

    protected GasDeviceState(GasDevice device) {
        this.device = device;
    }

    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now) {
        long secondsDifference = ChronoUnit.HOURS.between(lastSwitched, now);
        double hoursDifference = secondsDifference / 3600.0;
        return device.getGas() * hoursDifference;
    }

    protected void updateTotalConsumptionAndSwitch() {
        LocalDateTime now = LocalDateTime.now(CustomClock.getClock());
        double consumptionBetween = device.getState().getConsumptionBetween(device.getLastSwitched(), now);
        device.setTotalGasConsumption(device.getTotalGasConsumption() + consumptionBetween);
        device.setLastSwitched(now);
    }
}
