package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Cleaner extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    public Cleaner(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(EventType.MESSY_HOUSE, this);
        eventManager.subscribe(EventType.CLEAN_HOUSE, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case MESSY_HOUSE:
                this.getState().turnOn();
                log.info("Cleaner is cleaning the floor.");
                break;
            case CLEAN_HOUSE:
                this.getState().idle();
                log.info("Cleaner is done cleaning the floor.");
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
