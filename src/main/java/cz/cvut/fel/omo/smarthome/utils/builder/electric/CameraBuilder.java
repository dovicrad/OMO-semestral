package cz.cvut.fel.omo.smarthome.utils.builder.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.Camera;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CameraBuilder implements DeviceBuilder {
    String name;
    Integer wattage;

    @Override
    public CameraBuilder build(Director director) {
        director.buildCamera(this);
        return this;
    }

    @Override
    public CameraBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CameraBuilder setWattage(Integer wattage) {
        this.wattage = wattage;
        return this;
    }

    public Camera getResult() {
        log.info("A new camera was created by CameraBuilder.");
        return new Camera(name, wattage);
    }
}
