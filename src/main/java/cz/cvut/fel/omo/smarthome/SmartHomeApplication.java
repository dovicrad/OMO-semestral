package cz.cvut.fel.omo.smarthome;

import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.utils.SimulationManager;
import cz.cvut.fel.omo.smarthome.utils.factory.BigHouseGenerator;
import cz.cvut.fel.omo.smarthome.utils.factory.HouseGenerator;
import cz.cvut.fel.omo.smarthome.utils.factory.SimpleHouseGenerator;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * Entrypoint of the application.
 * <p>
 * This method creates a house and runs the simulation.
 * The type of house created depends on the config argument provided to the application:
 * <ul>
 *     <li>"simple" - creates a simple house</li>
 *     <li>"big" - creates a big house</li>
 *     <li>if no config is provided, a big house is created</li>
 *     <li>if config is null, a big house is created</li>
 *     <li>if config is invalid, a big house is created</li>
 * </ul>
 */
@Slf4j
public class SmartHomeApplication {
    public static void main(String[] args) {
        String config = args.length > 0 ? args[0] : null;

        HouseGenerator houseGenerator;

        if (Objects.isNull(config)) {
            houseGenerator = new BigHouseGenerator();
            log.info("No config found - creating a big house.");
        } else {
            switch (config) {
                case "simple":
                    houseGenerator = new SimpleHouseGenerator();
                    log.info("Creating a simple house.");
                    break;
                case "big":
                    houseGenerator = new BigHouseGenerator();
                    log.info("Creating a big house.");
                    break;
                default:
                    houseGenerator = new BigHouseGenerator();
                    log.info("Invalid config - creating a big house as default.");
                    break;
            }
        }

        House house = houseGenerator.createHouse();

        if(Objects.nonNull(house)){
            SimulationManager.simulate();
            house.getReportMaker().makeReport();
            house.getHouseConfigurationReport().generate();
        }else{
            log.error("House was not created.");
        }
    }
}
