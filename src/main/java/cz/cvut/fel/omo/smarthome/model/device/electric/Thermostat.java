package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Thermostat extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    public Thermostat(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(EventType.TURN_ON_AUTO_TEMPERATURE, this);
        eventManager.subscribe(EventType.TURN_OFF_AUTO_TEMPERATURE, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case TURN_ON_AUTO_TEMPERATURE:
                this.getState().turnOn();
                eventManager.subscribe(EventType.COLD, this);
                eventManager.subscribe(EventType.HOT, this);
                break;
            case TURN_OFF_AUTO_TEMPERATURE:
                this.getState().idle();
                eventManager.unsubscribe(EventType.COLD, this);
                eventManager.unsubscribe(EventType.HOT, this);
                break;
            case COLD:
                eventManager.notifySubscribers(EventType.TURN_ON_HEATING);
                eventManager.notifySubscribers(EventType.TURN_OFF_AIR_CONDITIONING);
                break;
            case HOT:
                eventManager.notifySubscribers(EventType.TURN_OFF_HEATING);
                eventManager.notifySubscribers(EventType.TURN_ON_AIR_CONDITIONING);
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
