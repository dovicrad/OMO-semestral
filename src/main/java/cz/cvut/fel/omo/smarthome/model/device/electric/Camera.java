package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Camera extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    public Camera(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(EventType.INTRUDER_CAME, this);
        eventManager.subscribe(EventType.INTRUDER_LEFT, this);
   }

    @Override
    public void update(EventType event) {
        switch (event){
            case INTRUDER_CAME:
                this.getState().turnOn();
                eventManager.notifySubscribers(EventType.ENABLE_HOUSE_SECURITY);
                log.info("Camera recording.");
                break;
            case INTRUDER_LEFT:
                eventManager.notifySubscribers(EventType.DISABLE_HOUSE_SECURITY);
                this.getState().idle();
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
