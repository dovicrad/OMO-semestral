package cz.cvut.fel.omo.smarthome.model.device;

public class FridgeItem {
    /**
     * Name of item
     */
    private final String itemName;

    /**
     *  Size of item, ranges from 0.01 to 1.00, where 1.00 is max size of fridge
     */
    private final double itemSize;

    /**
     * Constructor for fridge item
     * @param itemName name of item
     * @param itemSize size of item
     */
    public FridgeItem(String itemName, double itemSize) {
        this.itemName = itemName;
        this.itemSize = itemSize;
    }

    /**
     * @return name of item
     */
    public String getName() { return itemName; }

    /**
     * @return size of item
     */
    public double getSize() { return itemSize; }

}
