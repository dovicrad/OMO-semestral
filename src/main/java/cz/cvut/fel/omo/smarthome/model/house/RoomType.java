package cz.cvut.fel.omo.smarthome.model.house;

public enum RoomType {
    BEDROOM,
    BATHROOM,
    KITCHEN,
    LIVING_ROOM,
    TOILET,
    GARAGE,
    DINING_ROOM
}
