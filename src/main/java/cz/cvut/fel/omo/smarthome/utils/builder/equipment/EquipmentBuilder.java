package cz.cvut.fel.omo.smarthome.utils.builder.equipment;

import cz.cvut.fel.omo.smarthome.model.equipment.Color;

/**
 * This interface is a part of Builder design pattern.
 * It provides methods to build equipment.
 */
public interface EquipmentBuilder {
    EquipmentBuilder setColor (Color color);
    EquipmentBuilder setBrand (String brand);
}
