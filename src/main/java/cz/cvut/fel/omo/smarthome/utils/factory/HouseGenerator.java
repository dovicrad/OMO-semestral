package cz.cvut.fel.omo.smarthome.utils.factory;

import cz.cvut.fel.omo.smarthome.model.device.Device;
import cz.cvut.fel.omo.smarthome.model.house.Floor;
import cz.cvut.fel.omo.smarthome.model.house.House;
import cz.cvut.fel.omo.smarthome.model.house.Room;
import cz.cvut.fel.omo.smarthome.model.house.RoomType;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import cz.cvut.fel.omo.smarthome.utils.builder.electric.*;
import cz.cvut.fel.omo.smarthome.utils.builder.gas.GasBoilerBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.gas.HeaterBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Factory and Builder design patterns.
 *
 * This abstract class represents a HouseGenerator in the Factory method design pattern.
 * It provides a method to create a House with a specific number of floors, rooms per floor, devices, and device types.
 *
 */
@Slf4j
public abstract class HouseGenerator {
    protected final Director director;

    protected static final List<DeviceBuilder> BUILDERS = Arrays.asList(
            new CameraBuilder(),
            new CleanerBuilder(),
            new CurtainBuilder(),
            new DoorBuilder(),
            new FridgeBuilder(),
            new GasBoilerBuilder(),
            new HeaterBuilder(),
            new LightBuilder(),
            new TelevisionBuilder(),
            new ThermostatBuilder(),
            new AirConditioner(),
            new WindowBuilder()
    );

    protected HouseGenerator() {
        this.director = new Director();
    }

    public abstract House createHouse();

    /**
     * Creates a house with a specific number of floors, rooms per floor, devices, and device types.
     * @param numberOfFloors number of floors
     * @param numberOfRoomsPerFloor number of rooms per floor
     * @param numberOfDevices number of devices
     * @param numberOfDeviceTypes number of device types
     * @return house with the specified number of floors, rooms per floor, devices, and device types
     */
    protected House buildHouseWithDevices(Integer numberOfFloors, Integer numberOfRoomsPerFloor, Integer numberOfDevices, Integer numberOfDeviceTypes) {
        List<Device> allDevices = createDevices(numberOfDevices, numberOfDeviceTypes);
        Iterator<Device> deviceIterator = allDevices.iterator();

        int totalRooms = numberOfFloors * numberOfRoomsPerFloor;
        int devicesPerRoom = numberOfDevices / totalRooms;

        House house = new House();
        house.setFloors(IntStream.range(0, numberOfFloors)
                .mapToObj(i -> {
                    Floor floor = new Floor(house);
                    floor.setRooms(IntStream.range(0, numberOfRoomsPerFloor).mapToObj(j -> {
                        Room r = new Room(RoomType.BEDROOM, floor);
                        List<Device> roomDevices = new ArrayList<>();
                        for (int k = 0; k < devicesPerRoom && deviceIterator.hasNext(); k++) {
                            roomDevices.add(deviceIterator.next());
                        }
                        r.addDevice(roomDevices);
                        return r;
                    }).toList());
                    return floor;
                }).toList());
        return house;
    }

    /**
     * Creates a list of devices with a specific number of devices and device types.
     * @param numberOfDevices number of devices
     * @param numberOfDeviceTypes number of device types
     * @return list of devices with the specified number of devices and device types
     */
    private List<Device> createDevices(Integer numberOfDevices, Integer numberOfDeviceTypes) {
        if(BUILDERS.size() < numberOfDeviceTypes) {
            log.warn("There are not enough builders to create {} device types.", numberOfDeviceTypes);
        }

        List<Device> devices = new ArrayList<>();
        for (int i = 0; i < numberOfDevices; i++) {
            DeviceBuilder builder = BUILDERS.get(i % numberOfDeviceTypes);
            builder.build(this.director);
            devices.add(builder.getResult());
        }
        return devices;
    }
}
