package cz.cvut.fel.omo.smarthome.utils.builder.gas;

import cz.cvut.fel.omo.smarthome.model.device.gas.GasBoiler;
import cz.cvut.fel.omo.smarthome.utils.builder.DeviceBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.Director;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GasBoilerBuilder implements DeviceBuilder {
    String name;
    Integer gas;

    public GasBoilerBuilder setGas(Integer gas) {
        this.gas = gas;
        return this;
    }

    @Override
    public GasBoilerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public GasBoilerBuilder build(Director director) {
        director.buildGasBoiler(this);
        return this;
    }

    public GasBoiler getResult() {
        log.info("A new gas boiler was created by GasBoilerBuilder.");
        return new GasBoiler(name, gas);
    }
}
