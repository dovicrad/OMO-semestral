package cz.cvut.fel.omo.smarthome.model.resident;

import cz.cvut.fel.omo.smarthome.model.equipment.Equipment;
import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import cz.cvut.fel.omo.smarthome.model.house.Room;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

@Getter
@Slf4j
public class Person extends Resident implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    public Person(String name, Room locatedIn) {
        super(name, locatedIn);
        eventManager.subscribe(EventType.BAD_HOUSE_TEMPERATURE, this);
        eventManager.subscribe(EventType.NIGHT, this);
        eventManager.subscribe(EventType.RAINY, this);
        eventManager.subscribe(EventType.DAY, this);
        eventManager.subscribe(EventType.SUNNY, this);
    }

    @Override
    public void update(EventType event) {
        switch (event) {
            case NIGHT:
                log.info(getName() + " is turning on lights.");
                eventManager.notifySubscribers(EventType.TURN_ON_LIGHTS);
                break;
            case DAY:
                log.info(getName() + " is turning lights off.");
                eventManager.notifySubscribers(EventType.TURN_OFF_LIGHTS);
                break;
            case RAINY:
                log.info(getName() + " is going to watch TV.");
                eventManager.notifySubscribers(EventType.TURN_ON_TV);
                break;
            case BAD_HOUSE_TEMPERATURE:
                log.info(getName() + " is going to turn on thermostat.");
                eventManager.notifySubscribers(EventType.TURN_ON_AUTO_TEMPERATURE);
                break;
            case SUNNY:
                boolean decision = new java.util.Random().nextBoolean();
                if (decision) {
                    log.info(getName() + " is going to relax.");
                    eventManager.notifySubscribers(EventType.TURN_OFF_AUTO_CURTAINS);
                    eventManager.notifySubscribers(EventType.TURN_ON_TV);
                } else {
                    log.info(getName() + " is going to sport.");
                    findSummerSportingEquipment();
                }
                break;
            default:
                log.info("I cannot handle this event");
        }
    }

    private void findSummerSportingEquipment() {
        List<String> equipmentNames = Arrays.asList("Bike", "Ball");

        List<Equipment> available = this.getLocatedIn().getFloor().getHouse().getEquipment().stream()
                .filter(Equipment::isAvailable)
                .filter(equipment -> equipmentNames.stream().anyMatch(name -> name.equals(equipment.getEquipmentName()))).toList();
        if(available.isEmpty()){
            log.info("No equipment available");
            return;
        }
        Equipment target = available.get(0);
        target.setCurrentOwner(this);
    }


}
