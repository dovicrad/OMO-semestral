package cz.cvut.fel.omo.smarthome.model.device.state.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.ElectricDevice;
import cz.cvut.fel.omo.smarthome.model.device.state.DeviceState;
import cz.cvut.fel.omo.smarthome.utils.CustomClock;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class ElectricDeviceState implements DeviceState {
    ElectricDevice device;

    protected ElectricDeviceState(ElectricDevice device) {
        this.device = device;
    }

    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now) {
        long secondsDifference = ChronoUnit.HOURS.between(lastSwitched, now);
        double hoursDifference = secondsDifference / 3600.0;
        return device.getWattage() * hoursDifference;
    }

    protected void updateTotalConsumptionAndSwitch() {
        LocalDateTime now = LocalDateTime.now(CustomClock.getClock());
        double consumptionBetween = device.getState().getConsumptionBetween(device.getLastSwitched(), now);
        device.setTotalEnergyConsumption(device.getTotalEnergyConsumption() + consumptionBetween);
        device.setLastSwitched(now);
    }
}

