package cz.cvut.fel.omo.smarthome.model.resident;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import cz.cvut.fel.omo.smarthome.model.house.Room;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class Pet extends Resident implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    private final String kindOfAnimal;
    private final boolean hunger;
    private int hoursWithoutFood;

    public Pet(String name, int age, String kindOfAnimal, Room locatedIn) {
        super(name, locatedIn);
        this.kindOfAnimal = kindOfAnimal;
        this.hunger = false;
        eventManager.subscribe(EventType.HOUR_PASSED, this);
    }

    @Override
    public void update(EventType event) {
        switch (event) {
            case HOUR_PASSED -> {
                hoursWithoutFood++;
                if(hoursWithoutFood > 6) {
                    log.info(getKindOfAnimal() + " " + getName() + " was not fed and made a revenge.");
                    eventManager.notifySubscribers(EventType.MESSY_HOUSE);
                }
            }
            case HUNGRY -> {
                log.info(getKindOfAnimal() + " " + getName() + " get event " + event);
                getFood();
            }

            default -> log.info(getKindOfAnimal() + " " + getName() + " unable to get this event.");
        }
    }

    public boolean isHungry() {
        return hunger;
    }

    private void getFood() {
        log.info(getKindOfAnimal() + getName() + " eats");
    }
}
