package cz.cvut.fel.omo.smarthome.model.device.electric;

import cz.cvut.fel.omo.smarthome.model.event.EventManager;
import cz.cvut.fel.omo.smarthome.model.event.EventType;
import cz.cvut.fel.omo.smarthome.model.event.Subscriber;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Television extends ElectricDevice implements Subscriber {
    private static final EventManager eventManager = EventManager.getInstance();

    public Television(String deviceName, Integer wattage) {
        super(deviceName, wattage);
        eventManager.subscribe(EventType.TURN_ON_TV, this);
        eventManager.subscribe(EventType.TURN_OFF_TV, this);
    }

    @Override
    public void update(EventType event) {
        switch (event){
            case TURN_ON_TV:
                this.getState().turnOn();
                break;
            case TURN_OFF_TV:
                this.getState().turnOff();
                break;
            default:
                log.info("I cannot handle this event.");
        }
    }
}
