package cz.cvut.fel.omo.smarthome.model.device.state.electric;

import cz.cvut.fel.omo.smarthome.model.device.electric.ElectricDevice;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
public class ElectricIdleState extends ElectricDeviceState {
    private static final double IDLE_CONSUMPTION_FACTOR = 0.5;

    public ElectricIdleState(ElectricDevice device) {
        super(device);
    }

    @Override
    public double getConsumptionBetween(LocalDateTime lastSwitched, LocalDateTime now) {
        return super.getConsumptionBetween(lastSwitched, now) * IDLE_CONSUMPTION_FACTOR;
    }

    @Override
    public void turnOn() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to on");
        device.setState(new ElectricTurnedOnState(device));
    }

    @Override
    public void turnOff() {
        this.updateTotalConsumptionAndSwitch();
        log.info(device.getDeviceName() + " switched state to off");
        device.setState(new ElectricTurnedOffState(device));
    }

    @Override
    public void idle() {
        log.info(device.getDeviceName() + " is already idle");
    }
}
