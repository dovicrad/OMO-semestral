package cz.cvut.fel.omo.smarthome.utils.builder;

import cz.cvut.fel.omo.smarthome.utils.builder.electric.*;
import cz.cvut.fel.omo.smarthome.utils.builder.gas.GasBoilerBuilder;
import cz.cvut.fel.omo.smarthome.utils.builder.gas.HeaterBuilder;

public class Director {
    public void buildCamera(CameraBuilder cameraBuilder) {
        cameraBuilder.setName("Camera").setWattage(100);
    }

    public void buildCleaner(CleanerBuilder cleanerBuilder) {
        cleanerBuilder.setName("Cleaner").setWattage(100);
    }

    public void buildCurtain(CurtainBuilder curtainBuilder) {
        curtainBuilder.setName("Curtain").setWattage(100);
    }

    public void buildDoor(DoorBuilder doorBuilder) {
        doorBuilder.setName("Door").setWattage(100);
    }

    public void buildFridge(FridgeBuilder fridgeBuilder) {
        fridgeBuilder.setName("Fridge").setWattage(100);
    }

    public void buildGasBoiler(GasBoilerBuilder gasBoilerBuilder) {
        gasBoilerBuilder.setName("Gas boiler").setGas(100);
    }

    public void buildHeater(HeaterBuilder heaterBuilder) {
        heaterBuilder.setName("Heater").setGas(100);
    }

    public void buildLight(LightBuilder lightBuilder) {
        lightBuilder.setName("Light").setWattage(100);
    }

    public void buildTelevision(TelevisionBuilder televisionBuilder) {
        televisionBuilder.setName("Television").setWattage(100);
    }

    public void buildThermostat(ThermostatBuilder thermostatBuilder) {
        thermostatBuilder.setName("Thermostat").setWattage(100);
    }

    public void buildAirConditioner(AirConditioner airConditioner) {
        airConditioner.setName("Air conditioner").setWattage(100);
    }

    public void buildWindow(WindowBuilder windowBuilder) {
        windowBuilder.setName("Window").setWattage(100);
    }
}